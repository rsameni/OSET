# The Open-Source Electrophysiological Toolbox (OSET)

OSET has migrated to GitHub: [https://github.com/alphanumericslab/OSET.git](https://github.com/alphanumericslab/OSET.git)
